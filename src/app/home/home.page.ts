import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  emprestimos: any[]=[];
  monitor: number =10;
  notebook: number =10;
  projetor: number =10;
  constructor(private alertCtrl : AlertController) {
    let emprestmosJson = localStorage.getItem('emprestimosBD');
    if (emprestmosJson != null){
      this.emprestimos = JSON.parse(emprestmosJson);
    }
  }
  async emprestaMonitor(){
    const alert = await this.alertCtrl.create({
      header: 'Emprestimo',
      message: '<strong>Confirma eprestimo de monitor?</strong>!!!',
      inputs:[
        {
          name: 'tipo',
          type: 'radio',
          label: 'Monitor',
          value: 'monitor',
          checked: true
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'danger',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Confirmar',
          handler: async (form) => {
            var i:number;
            for (i=0;i<this.emprestimos.length;i++){
              if(this.emprestimos[i]['name'] == form) {
                const alert = await this.alertCtrl.create({
                  header: 'Emprestimo',
                  message: '<strong>Você já possui um '+form+' emprestado! Favor devolver '+form+' equipamento</strong>!!!',
                  buttons: [
                    {
                      text: 'Okay',
                      handler: () => {
                      }
                    }
                  ]
                });
                await alert.present();
                return;
              }
            }
            this.monitor=9;
            this.add(form);
          }
        }
      ]
    });

    await alert.present();
  }
  async emprestaNotebook(){
    const alert = await this.alertCtrl.create({
      header: 'Emprestimo',
      message: '<strong>Confirma eprestimo de notebook?</strong>!!!',
      inputs:[
        {
          name: 'tipo',
          type: 'radio',
          label: 'Notebook',
          value: 'notebook',
          checked: true
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'danger',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Confirmar',
          handler: async (form) => {
            var i:number;
            for (i=0;i<this.emprestimos.length;i++){
              if(this.emprestimos[i]['name'] == form) {
                const alert = await this.alertCtrl.create({
                  header: 'Emprestimo',
                  message: '<strong>Você já possui um '+form+' emprestado! Favor devolver '+form+' equipamento</strong>!!!',
                  buttons: [
                    {
                      text: 'Okay',
                      handler: () => {
                      }
                    }
                  ]
                });
                await alert.present();
                return;
              }
            }
            this.notebook=9;
            this.add(form);
          }
        }
      ]
    });

    await alert.present();
  }

  async emprestaProjetor(){
    const alert = await this.alertCtrl.create({
      header: 'Emprestimo',
      message: '<strong>Confirma eprestimo de projetor?</strong>!!!',
      inputs:[
        {
          name: 'tipo',
          type: 'radio',
          label: 'Projetor',
          value: 'projetor',
          checked: true
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'danger',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Confirmar',
          handler: async (form) => {
            var i:number;
            for (i=0;i<this.emprestimos.length;i++){
              if(this.emprestimos[i]['name'] == form) {
                const alert = await this.alertCtrl.create({
                  header: 'Emprestimo',
                  message: '<strong>Você já possui um '+form+' emprestado! Favor devolver '+form+' equipamento</strong>!!!',
                  buttons: [
                    {
                      text: 'Okay',
                      handler: () => {
                      }
                    }
                  ]
                });
                await alert.present();
                return;
              }
            }
            this.projetor=9;
            this.add(form);
          }
        }
      ]
    });

    await alert.present();
  }
  async add(equipamentoEmprestado : string){
    let equipamento = {name: equipamentoEmprestado};
    this.emprestimos.push(equipamento);
    this.updateLocalStorage();
  }
  devolver(equipamento: any){
    this.emprestimos = this.emprestimos.filter(equipamentoArray => equipamento != equipamentoArray);
    this.updateLocalStorage();
    if (equipamento['name'] == "monitor"){
      this.monitor=10;
    }
    if (equipamento['name'] == "notebook"){
      this.notebook=10;
    }
    if (equipamento['name'] == "projetor"){
      this.projetor=10;
    }
  }

  updateLocalStorage(){
    localStorage.setItem('emprestimosBD', JSON.stringify(this.emprestimos));
  }
}
